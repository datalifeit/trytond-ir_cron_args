# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class IrCronArgsTestCase(ModuleTestCase):
    """Test Ir Cron Args module"""
    module = 'ir_cron_args'


del ModuleTestCase
