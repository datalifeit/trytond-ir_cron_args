# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from ast import literal_eval
from trytond.model import fields, ModelView, dualmethod
from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    name_ = fields.Char('Name')
    args_ = fields.Char('Arguments')

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 5.0: rename fields
        if table_h.column_exist('name'):
            table_h.column_rename('name', 'name_')
        table_h.not_null_action('name_', action='remove')
        if table_h.column_exist('args'):
            table_h.column_rename('args', 'args_')
        table_h.not_null_action('args_', action='remove')

        super().__register__(module_name)

    @dualmethod
    @ModelView.button
    def run_once(cls, crons):
        for cron in crons:
            if not getattr(cron, 'companies', []):
                cron._run_once()
            else:
                for company in cron.companies:
                    with Transaction().set_context(company=company.id):
                        cron._run_once()

    def _run_once(self):
        model, method = self.method.split('|')
        Model = Pool().get(model)
        if self.args_:
            args = literal_eval(self.args_)
        else:
            args = []
        getattr(Model, method)(*args)
