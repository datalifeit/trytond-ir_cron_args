datalife_ir_cron_args
=====================

The ir_cron_args module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-ir_cron_args/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-ir_cron_args)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
